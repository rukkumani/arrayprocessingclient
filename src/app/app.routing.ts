import { RouterModule, Routes } from "@angular/router";
import { LoginComponent } from "./login-component/login.component";
import { HomeComponent } from "./home-component/home.component";
import { ArrayMissingComponent } from "./array-missing-component/array-missing.component";
import { ArrayOrderComponent } from "./array-order-component/array-order.component";
import { StringReverseComponent } from "./string-component/string-reverse.component";
const mainRoutes: Routes = [
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  },
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "home",
    component: HomeComponent,

    // redirectTo: 'adminHome',
    children: [
      {
        path: "arrayOrder",
        component: ArrayOrderComponent,
        outlet: "event"
      },
      {
        path: "stringReverse",
        component: StringReverseComponent,
        outlet: "event"
      },
      {
        path: "missingNumber",
        component: ArrayMissingComponent,
        outlet: "event"
      }
    ]
  },
  {
    path: "**",
    redirectTo: "login"
  }
];
export const mainRouting = RouterModule.forRoot(mainRoutes, {
  useHash: true,
  onSameUrlNavigation: "reload"
});
