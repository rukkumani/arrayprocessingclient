import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";

@Injectable()
export class ArrayService {
  server = "http://localhost:4001/";
  userData = [];
  constructor(private http: HttpClient) {}
  orderNumber(inputArrayForOrder): Observable<any> {
    return this.http.post(
      this.server + `arrayAlteration/TYPE=1`,
      inputArrayForOrder
    );
  }

  reverseString(inputStrForReverse): Observable<any> {
    return this.http.post(
      this.server + `stringAlteration/TYPE=2`,
      inputStrForReverse
    );
  }

  findMissedArrNumber(inputArray): Observable<any> {
    return this.http.post(
      this.server + `findMissingElement/TYPE=3`,
      inputArray
    );
  }
}
