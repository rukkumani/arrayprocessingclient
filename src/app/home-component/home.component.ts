import { Component, OnInit } from "@angular/core";
import { ArrayService } from "../services/array.services";
import { ToasterService } from "angular5-toaster/angular5-toaster";
import { Router } from "@angular/router";

@Component({
  selector: "app-home-component",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  constructor(
    private toaster: ToasterService,
    private ArrayService: ArrayService,
    private router: Router
  ) {}
  ngOnInit() {}
}
