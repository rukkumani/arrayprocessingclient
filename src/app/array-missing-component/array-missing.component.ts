import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ArrayService } from "../services/array.services";
import { ToasterService } from "angular5-toaster/angular5-toaster";

@Component({
  selector: "app-array-missing-component",
  templateUrl: "./array-missing.component.html",
  styleUrls: ["./array-missing.component.css"]
})
export class ArrayMissingComponent {
  arrayElement: number[] = [];
  arrayNumber: number;
  missingArr: number[];
  constructor(
    private router: Router,
    private toaster: ToasterService,
    private ArrayService: ArrayService
  ) {}

  addNumber(inNumber) {
    if (
      inNumber &&
      typeof inNumber == "number" &&
      !isNaN(inNumber) &&
      isFinite(inNumber)
    ) {
      this.arrayElement.push(inNumber);
      this.arrayNumber = undefined;
    } else {
      this.toaster.pop("warning", "Please provide number");
    }
  }

  findMissedArrNumber() {
    if (this.arrayElement && this.arrayElement.length) {
      this.ArrayService.findMissedArrNumber({
        inputArray: this.arrayElement
      }).subscribe(data => {
        if (
          data &&
          data.data == "success" &&
          data.result &&
          data.result.length
        ) {
          this.missingArr = data.result;
          this.arrayElement = [];
        } else if (data.data == "redirect") {
          this.router.navigate(["login"]);
        } else {
          this.missingArr = [];
          this.arrayElement = [];
          this.toaster.pop(
            "warning",
            "unable to process your request!.Please provide correct value"
          );
        }
      });
    }
  }
}
