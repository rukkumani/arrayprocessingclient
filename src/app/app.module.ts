import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BrowserModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { MentionModule } from "angular-mentions/mention";
import { mainRouting } from "./app.routing";
import { ArrayService } from "./services/array.services";
import {
  ToasterModule,
  ToasterService
} from "angular5-toaster/angular5-toaster";
import { FormsModule } from "@angular/forms";

/*component*/
import { AppComponent } from "./app.component";
import { HomeComponent } from "./home-component/home.component";
import { LoginComponent } from "./login-component/login.component";
import { ArrayMissingComponent } from "./array-missing-component/array-missing.component";
import { ArrayOrderComponent } from "./array-order-component/array-order.component";
import { StringReverseComponent } from "./string-component/string-reverse.component";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ArrayMissingComponent,
    ArrayOrderComponent,
    StringReverseComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    mainRouting,
    FormsModule,
    ToasterModule,
    MentionModule
  ],
  providers: [ArrayService, ToasterService],
  bootstrap: [AppComponent]
})
export class AppModule {}
