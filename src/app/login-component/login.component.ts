import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ArrayService } from "../services/array.services";
import { ToasterService } from "angular5-toaster/angular5-toaster";


@Component({
  selector: "app-login-component",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent {
  constructor(
    private router: Router,
    private toaster: ToasterService,
    private ArrayService: ArrayService
  ) {}

  userLogin() {}
}
