import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ArrayService } from "../services/array.services";
import { ToasterService } from "angular5-toaster/angular5-toaster";

@Component({
  selector: "app-array-order-component",
  templateUrl: "./array-order.component.html",
  styleUrls: ["./array-order.component.css"]
})
export class ArrayOrderComponent {
  arrayElement: number[] = [];
  arrayNumber: number;
  reversedArr: number[];
  constructor(
    private router: Router,
    private toaster: ToasterService,
    private ArrayService: ArrayService
  ) {}

  addNumber(inNumber) {
    if (
      inNumber &&
      typeof inNumber == "number" &&
      !isNaN(inNumber) &&
      isFinite(inNumber)
    ) {
      this.arrayElement.push(inNumber);
      this.arrayNumber = undefined;
    } else {
      this.toaster.pop("warning", "Please provide Only number");
    }
  }

  orderArrNumber() {
    if (this.arrayElement && this.arrayElement.length) {
      this.ArrayService.orderNumber({
        modifyArray: this.arrayElement
      }).subscribe(data => {
        if (
          data &&
          data.data == "success" &&
          data.result &&
          data.result.length
        ) {
          this.reversedArr = data.result;
          this.arrayElement = [];
        } else if (data.data == "redirect") {
          this.router.navigate(["login"]);
        } else {
          this.arrayElement = [];
          this.reversedArr = [];
          this.toaster.pop("warning", "unable to process your request!");
        }
      });
    }
  }
}
