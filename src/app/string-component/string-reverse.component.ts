import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ArrayService } from "../services/array.services";
import { ToasterService } from "angular5-toaster/angular5-toaster";

@Component({
  selector: "app-string-reverse-component",
  templateUrl: "./string-reverse.component.html",
  styleUrls: ["./string-reverse.component.css"]
})
export class StringReverseComponent {
  inputString: string;
  outputString;
  constructor(
    private router: Router,
    private toaster: ToasterService,
    private ArrayService: ArrayService
  ) {}

  reverseString() {
    if (this.inputString) {
      this.ArrayService.reverseString({
        stringInput: this.inputString
      }).subscribe(data => {
        if (data && data.data == "success" && data.result) {
          this.toaster.pop("success", "Successfully reversed the string");

          this.outputString = data.result;
        } else if (data.data == "redirect") {
          this.router.navigate(["login"]);
        } else {
          this.outputString = "";
          this.toaster.pop("warning", " Sorry Unable to process your string");
        }
      });
    } else {
      this.toaster.pop("warning", "please Provide the string");
    }
  }
}
